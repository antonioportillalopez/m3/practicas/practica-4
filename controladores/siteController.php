<?php
namespace controladores;

class siteController extends Controller{
    
    public function indexAccion($objeto){
      $this->render([
          "vista"=>"index",
          "pie"=>"Estamos probando",
          "menu"=>(new \clases\Menu([
            "Inicio"=>$this->crearRuta(["accion"=>"index"]),
            "1"=>$this->crearRuta(["controlador"=>"cuatro","accion"=>"one"]),
            "2"=>$this->crearRuta(["controlador"=>"cuatro","accion"=>"two"]),
            "3"=>$this->crearRuta(["controlador"=>"cuatro","accion"=>"three"]),
            "5"=>$this->crearRuta(["controlador"=>"cuatro","accion"=>"listarfive"]),
            "6"=>$this->crearRuta(["controlador"=>"cuatro","accion"=>"six"]),
        ],"Inicio"))->html()
    ]);
    }
    

}
