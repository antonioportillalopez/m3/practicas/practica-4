<?php

namespace controladores;
use clases\Estadistica;


class cuatroController extends Controller {

    public function oneAccion($objeto) {
        $this->render([
            "vista" => "primero",
            "pie" => "primer ejercicio",
            "contenido" => empty($objeto->getValores()) ? ["no hay nada"] : $objeto->getValores(),
            "menu" => (new \clases\Menu([
                                            "Inicio" => $this->crearRuta(["accion" => "index"]),
                                            "Ejercicio 1" => $this->crearRuta(["controlador" => "cuatro", "accion" => "one"]),
                                        ], 
                                        "Ejercicio 1"))->html(),
        ]);
    }

    public function listaroneAccion($objeto) {
        $this->render([
            "vista" => "listarprimero",
            "pie" => "Salen los numeros ordenados",
            "contenido" => empty($objeto->getValores()) ? ["no hay nada"] : $objeto->getValores(),
            "menu" => (new \clases\Menu([
                "Inicio" => $this->crearRuta(["accion" => "index"]),
                "Listado ejercicio1" => $this->crearRuta(["controlador" => "cuatro","accion" => "listarone"]),
                "ejercicio 1" => $this->crearRuta(["controlador" => "cuatro","accion" => "one"])
                    ], "Listado ejercicio1"))->html()
        ]);
    }

    public function twoAccion() {
        $this->render([
            "vista" => "segundo",
            "pie" => "ejercicio 2",
            "menu" => (new \clases\Menu([
                "Inicio" => $this->crearRuta(["accion" => "index"]),
                "Ejercicio 2" => $this->crearRuta(["controlador" => "cuatro", "accion" => "two"]),
                    ], "Ejercicio 2"))->html()
        ]);
    }
    
        public function listartwoAccion($objeto) {
        $this->render([
            "vista" => "listarsegundo",
            "pie" => "Cuenta vocales",
            "contenido" => empty($objeto->getValores()) ? ["no hay nada"] : $objeto->getValores(),
            "menu" => (new \clases\Menu([
                "Inicio" => $this->crearRuta(["accion" => "index"]),
                "Listado ejercicio2" => $this->crearRuta(["controlador" => "cuatro","accion" => "listartwo"]),
                "ejercicio 2" => $this->crearRuta(["controlador" => "cuatro","accion" => "two"])
                    ], "Listado ejercicio2"))->html()
        ]);
    }
    
    public function threeAccion() {
        $this->render([
            "vista" => "tercero",
            "pie" => "ejercicio 3",
            "menu" => (new \clases\Menu([
                "Inicio" => $this->crearRuta(["accion" => "index"]),
                "Ejercicio 3" => $this->crearRuta(["controlador" => "cuatro", "accion" => "three"]),
                    ], "Ejercicio 3"))->html()
        ]);
    }
    
    
    
        public function listarthreeAccion($objeto) {
            
            $o=new Estadistica($objeto->getValores()['numero']);
        
        
        $this->render([
            "vista" => "listartercero",
            "pie" => "Se han calculado: media, moda, mediana, desviaciación",
            "contenido" => $o->getEstadisticas(),
            "numeros"=> $o->getNumeros(),        
            "menu" => (new \clases\Menu([
                "Inicio" => $this->crearRuta(["accion" => "index"]),
                "Listado ejercicio3" => $this->crearRuta(["controlador" => "cuatro","accion" => "listarthree"]),
                "ejercicio 3" => $this->crearRuta(["controlador" => "cuatro","accion" => "three"])
                    ], "Listado ejercicio3"))->html()
        ]);
    }
    
    public function listarfiveAccion() {
            
            $o=new \clases\Diadelasemana();
        
        
        $this->render([
            "vista" => "listarfive",
            "pie" => "indica el dia de la semana en el que estamos",
            "contenido" => $o->getDia(),
            "numeros"=> $o->getDia(),        
            "menu" => (new \clases\Menu([
                "Inicio" => $this->crearRuta(["accion" => "index"]),
                "ejercicio 5" => $this->crearRuta(["controlador" => "cuatro","accion" => "listarfive"])
                    ], "ejercicio 5"))->html()
        ]);
    }
    
    
    
     public function sixAccion() {
        $this->render([
            "vista" => "six",
            "pie" => "ejercicio 6",
            "menu" => (new \clases\Menu([
                "Inicio" => $this->crearRuta(["accion" => "index"]),
                "Ejercicio 6" => $this->crearRuta(["controlador" => "cuatro", "accion" => "six"]),
                    ], "Ejercicio 6"))->html()
        ]);
    }
    
    
    
    
    public function listarsixAccion($objeto) {
        
                       
        $numero=$objeto->getValores();
        
        if (!isset($numero['numero']['0'])){
            $latabla='';
            
        }else{
            $latabla=$objeto->getValores()['numero'];
        }
            
            $o=new \clases\Tablamultiplicar($latabla);


        
        $this->render([
            "vista" => "listarsix",
            "pie" => 'Esta es la tabla de multiplicar del '.$o->getLadel(),
            "operador1"=> $o->getLadel(),        
            "operador2"=> [0,1,2,3,4,5,6,7,8,9,10],
            "resultado" => $o->getResul(),
            "menu" => (new \clases\Menu([
                "Inicio" => $this->crearRuta(["accion" => "index"]),
                "ejercicio 6" => $this->crearRuta(["controlador" => "cuatro","accion" => "listarsix"])
                    ], "ejercicio 6"))->html()
        ]);
    }
    
    

}
