<?php
namespace modelos;

class estadisticas {
    private $valores;
    private $media;
    
    function getValores() {
        return $this->valores;
    }

    function getMedia() {
        return $this->media;
    }

    function setValores($valores): void {
        $this->valores = $valores;
    }

    function setMedia($media): void {
        $this->media = $media;
    }


    
}
