<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace clases;

/**
 * Description of Estadisticas
 *
 * @author antpo
 */
class Estadistica {
    
    private $estadisticas=[]; // cada funcion guarda el resultado en esta propiedad como array con el nombre del indice con el valor de la estadistica
    public $numeros;
    
    
    public function __construct($valoresformulario) {
            
            $this->numeros=$valoresformulario; // recibe valores como array numerico y los guarda en la propiedad numeros
            //$this->setEstadisticas($this->media($valoresformulario));
            $this->media(); //ejecuta los procedimientos al instanciar la clase, ver que cada procedimiento guarda la propiedad
            $this->moda();
            $this->mediana();
            $this->desviacion();
            
    }
    
    
    public function suma() { //este procedimiento suma los valores recibidos del array y devuelve la suma en un valor
        $suma=0;

            foreach ($this->numeros as $value)
            {
                $suma=$value+$suma;
            }
        return $suma;
        
    }

        public function desviacion(){
        
        $suma=$this->suma();
        
        $media=$this->media();
        
        $numdatos=count($this->numeros);
        
        $desviacion='';
            foreach ($this->numeros as $key=>$value)
            {
                $desviacion.='<br>'. $value.'= '. sqrt(($suma-pow(($value-$media),2)/$numdatos));
            }
        
        $this->estadisticas['DESVIACION']=$desviacion;
        
    }
    
    
    public function mediana(){
        $num_elementos=count($this->numeros);
        $lugar_medio=floor($num_elementos/2);//el array empieza con 0 a si que en relidad el valor medio es la posición de la división sin sumar 1
        $resultado=0;
        $enorden=$this->numeros;
        sort($enorden);
        
        if ($num_elementos%2==0){
            
            $resultado=($enorden[$lugar_medio-1]+$enorden[$lugar_medio])/2;
            
        }else{
            
            $resultado=$enorden[$lugar_medio];
            
        };
        
        $this->estadisticas['mediana']=$resultado;
        
    }
    
    
    
    public function media(){
            $suma=$this->suma();
            $media=$suma/count($this->numeros);
            $this->estadisticas['media']=$media;
            return $media;
    }
    
    public function moda(){
        
        $componentes=$this->numeros;
        
        // ------- mi moda la cual me rompí la cabeza para realizarla ------
        asort($componentes); //ordena el array numeros en posiciones de memoria consecutivos por valor.

        $ordenados[]='';
        $indice=0;

        foreach ($componentes as $componente){
            $ordenados[$indice]=$componente; //realiza un nuevo array numérico con los indices ordenados segun orden alfabetico del valor de numeros
            $indice++;
        }


        $veces=0;
        $desfase=0;


        for ($id=0; $id<count($ordenados)-1; $id++): //recorre el array numeros ordenados

            $veces=$id;

            while ( $id<=count($ordenados)-2 && $ordenados[$id]==$ordenados[$id+1]){ //si los valores contiguos son iguales pasa el indice al siguiente
                $id++;
            }

            $desfase=($id-$veces)+1; //se anota el desfase de indice 

            if ($desfase>1){
                $moda[$ordenados[$id]]=$desfase;

            }
        endfor;
        
        $resultadomodaenunicostring='';
        
        if (isset($moda)){ 

                foreach($moda as $key => $valor){

                    if (max($moda)==$valor){
                        $serepite=$valor;
                        $resultadomodaenunicostring.= '<br>'.'moda: '.$key;
                    }
                }$resultadomodaenunicostring.='<br> se repite '.$serepite.' veces';
        } else {
                $resultadomodaenunicostring="no hay moda";
        }
        
        $this->estadisticas['MODA']=$resultadomodaenunicostring;
        // --------------------
        
        
        
        
    }

    function getNumeros() {
        return $this->numeros;
    }

        
    
    function getEstadisticas() {
        return $this->estadisticas;
    }
    
    
}
