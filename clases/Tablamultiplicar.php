<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace clases;

/**
 * guarda como propiedades ladel corresponde a la tabla de multiplicar de loque tiene almacenado la variable ladel que es el operador1
 * y en el array resultado el indice corresponde al operador2 y el resultado el total. 
 * es decir queda $ladel multiplicado por ['indice de resultado'] = valor de $resultado
 */
class Tablamultiplicar { // 
    
    private $ladel;
    private $resul; 
            
    public function __construct($ladelnumero) {
        
        $ladelnumero==''?  $this->setLadel(rand(1, 100)) : $this->setLadel($ladelnumero);
        
        $operador1=$this->getLadel();
        
        $resultado[]=0;
        
        for ($operador2=1; $operador2<=10;$operador2++){
            $resultado[$operador2]=$operador1*$operador2;
        }
        $this->setResul($resultado);
        
    }
    
    function getResul() {
        return $this->resul;
    }

    function setResul($resultado): void {
        $this->resul = $resultado;
    }

    function setLadel($ladel): void {
        $this->ladel = $ladel;
    }
    
    function getLadel() {
        return $this->ladel;
    }

    
        //put your code here
}
