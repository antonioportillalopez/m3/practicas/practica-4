<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace clases;

/**
 * Description of Diadelasemana
 *
 * @author antpo
 */
class Diadelasemana {
    private $dia;
    
    public function __construct() {
       switch (date("w")){
           case 0:
                $this->dia=["Domingo"];
                break;
            case 1:
                $this->dia=["Lunes"];
                break;
            case 2:
                $this->dia=["Martes"];
                break;
            case 3:
                $this->dia=["Miércoles"];
                break;
            case 4:
                $this->dia=["Jueves"];
                break;
            case 5:
                $this->dia=["Viernes"];
                break;
            case 6:
                $this->dia=["Sábado"];
                break;
        }
    }
    
    
    function getDia() {
        return $this->dia;
    }

}
